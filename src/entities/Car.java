package entities;

public class Car extends Vehicles{

private String engineType;
public Car(String brand, String model, int registrationNum, String engineType)
{
	super(registrationNum,brand,model);
	this.engineType = engineType;
}
public Car()
{
	
}
public String getEngineType()
{
    return this.engineType;
}
public void setEngineType(String engineType)
{
    this.engineType = engineType;
}
@Override
public boolean equals(Object obj)
{
	Car c1=new Car("Hyundai","i20",5566,"Petrol");
	if(obj == null) {
		return false;
	}
	if(!(obj instanceof Car)) {
		return false;
	}
	if(obj == this) {
		return true;
	}
	return c1.getEngineType() == ((Car) obj).getEngineType();
}
@Override
public int hashCode() {
	int hash = 7;
	hash = 31 * hash +  engineType.hashCode();
	
	
	return hash;
}
}