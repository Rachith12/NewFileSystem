package file;

public class Inventory
{
	private String item;
	private int qty;
	private int price;
	private String Brand;
	private String model;

	public Inventory(String item, int qty, float tempPrice, String Brand, String model)
	{
		this.item = item;
		this.qty = qty;
		this.price = (int) tempPrice;
		this.Brand=Brand;
		this.model=model;
	}

	public float getTotal()
	{
		return qty * price;
	}

	public String toString()
	{
		return "Item: " + item + "\n" + "Quantity: " + qty + "\n"
					+ "Price: " + price + "\n" + "Brand: " +Brand + "\n" + "Model: " + model;
	}
}