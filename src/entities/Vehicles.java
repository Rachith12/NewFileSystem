package entities;
public abstract class Vehicles
{
	private int registrationNum;
	private String model;
	private String brand;
	
	public Vehicles() {
	}
	public String getModel() {
		return this.model;
	}
	
	public void setModel(String model) {
		this.model = model;
	}
	
	public int getRegistrationNum() {
		return this.registrationNum;
	}
	
	public void setRegn(int registrationNum) {
		this.registrationNum = registrationNum;
	}
	
	public String getBrand() {
		return this.brand;
	}
	
	public void setBrand(String brand) {
		this.brand = brand;
	}
	
	public Vehicles (int registrationNum,String brand,String model){
		this.model=model;
		this.brand=brand;
		this.registrationNum=registrationNum;
	}
	@Override
	public int hashCode() {
		int hash=7;
		hash = 31 * hash + registrationNum;
		hash = 31 * hash + model.hashCode();
		hash = 31 * hash + brand.hashCode();
		return hash;
	}
}
