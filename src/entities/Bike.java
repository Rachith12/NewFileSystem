package entities;

@SuppressWarnings("unused")
public class Bike extends Vehicles {
	private int numberOfWheels;
public Bike (String brand, String model, int registrationNum, int numberOfWheels){
    super(registrationNum,brand,model);
    this.numberOfWheels = numberOfWheels;
}
public Bike()
{
	
}
public int getNumberOfWheels()
{
    return this.numberOfWheels;
}
public void getModel(int numberOfWheels){
    this.numberOfWheels = numberOfWheels;
}
}