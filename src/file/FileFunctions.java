package file;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

import entities.Car;
import entities.Inventory;

public class FileFunctions
{
	public static void main( String [] args )
	{
		List<Inventory> invItem = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new FileReader("Vehicles.txt"));)
		{
			
			String fileRead = br.readLine();
			while (fileRead != null)
			{
				String[] tokenize = fileRead.split(",");
				String tempItem = tokenize[0];
				String brand = tokenize[1];
				String model = tokenize[2];
				int tempQty = Integer.parseInt(tokenize[3]);
				float tempPrice = Float.parseFloat(tokenize[4]);
				Inventory tempObj = new Inventory(tempItem, tempQty, tempPrice, brand, model);
				invItem.add(tempObj);
				fileRead = br.readLine();
			}
			
		}
		catch (FileNotFoundException f)
		{
			System.out.println("file not found");
		}
		catch (IOException ex)
		{
			ex.printStackTrace();
		}
		for (Inventory each : invItem)
		{
			System.out.println("====================");
			System.out.println(each);
			System.out.println();
			System.out.printf("Total value of Garage = %8.2f %n", each.getTotal());
		}
		Car c1=new Car("Hyundai","i20",9928,"Petrol");
		System.out.println("\nHashcode of c1 = " + c1.hashCode());
		Car c2=new Car("Lambirghini","Huracan",6677,"Petrol");
		System.out.println("Hashcode of c2 = " + c2.hashCode());
		System.out.println("c1 and c2 are they same ? " + c1.equals(c2));
	}
}

