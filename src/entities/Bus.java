package entities;

public class Bus extends Vehicles{

	private int numberOfPassengers;
public Bus (String brand, String model, int registrationNum, int numberOfPassengers)
{
	 super(registrationNum,brand,model);
	 this.numberOfPassengers=numberOfPassengers;
}
public Bus()
{
	
}
public int getNumberOfPassengers()
{
    return this.numberOfPassengers;
}
public void setNumberOfPassengers(int numberOfPassengers)
{
    this.numberOfPassengers = numberOfPassengers;
}
}
